import createPersistedState from 'use-persisted-state';

const useTokenState = createPersistedState<string | null>('token');
export default useTokenState;
