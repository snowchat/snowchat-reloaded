import type { NextPage } from 'next';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';
import useTokenState from '../shared/tokenState';

interface WhoamiData {
  email_address: string;
  id: bigint;
  is_developer: boolean;
  phone_number: string | null;
  username: string;
}

const App: NextPage = () => {
  const router = useRouter();
  const [token, setToken] = useTokenState(null);

  if (token == null) {
    router.push('/');
    return <></>;
  }

  const [data, setData] = useState<WhoamiData | null>(null);

  useEffect(() => {
    fetch('http://localhost:8080/api/v0/auth/check', {
      headers: {
        Authorization: token,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        setData(data);
      });
  }, []);

  return (
    <section>
      <h2 className="text-xl">Whoami</h2>
      {data == null ? (
        'Loading'
      ) : (
        <table>
          <thead className="text-xs font-semibold uppercase text-zinc-400">
            <tr>
              <th className="p-2 whitespace-nowrap">
                <div className="font-semibold text-left">Username</div>
              </th>
              <th className="p-2 whitespace-nowrap">
                <div className="font-semibold text-left">E-Mail</div>
              </th>
              <th className="p-2 whitespace-nowrap">
                <div className="font-semibold text-left">Phone number</div>
              </th>
              <th className="p-2 whitespace-nowrap">
                <div className="font-semibold text-left">Developer?</div>
              </th>
            </tr>
          </thead>
          <tbody className="text-sm divide-y divide-gray-600">
            <tr>
              <td className="p-2 whitespace-nowrap">
                <div className="font-medium text-gray-100">{data.username}</div>
              </td>
              <td className="p-2 whitespace-nowrap">
                <div className="text-left text-gray-300">{data.email_address}</div>
              </td>
              <td className="p-2 whitespace-nowrap">
                {data.phone_number !== null ? <div className="font-medium text-gray-300">{data.phone_number}</div> : <div className="font-medium text-gray-500">-</div>}
              </td>
              <td className="p-2 whitespace-nowrap">
                {data.is_developer ? <div className="font-medium text-green-500">Yes! 🎉</div> : <div className="font-medium text-red-500">No ❌</div>}
              </td>
            </tr>
          </tbody>
        </table>
      )}
    </section>
  );
};

export default App;
