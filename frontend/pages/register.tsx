import type { NextPage } from 'next';
import { useRouter } from 'next/router';
import useTokenState from '../shared/tokenState';

interface RegisterFormElement extends HTMLFormElement {
  username: HTMLInputElement;
  password: HTMLInputElement;
  email: HTMLInputElement;
}

const Register: NextPage = () => {
  const router = useRouter();
  const [token, setToken] = useTokenState(null);

  if (token != null) {
    router.push('/app');
    return <></>;
  }

  const registerUser = async (event: React.FormEvent<RegisterFormElement>) => {
    event.preventDefault();
    let el = event.target as RegisterFormElement;
    const res = await fetch('http://localhost:8080/api/v0/auth/register', {
      body: JSON.stringify({
        username: el.username.value,
        password: el.password.value,
        email_address: el.email.value,
      }),
      headers: {
        'Content-Type': 'application/json',
      },
      method: 'POST',
    });

    //window.localStorage.setItem('token', (await res.json()).token);
    let jsonResponse = await res.json();
    if (jsonResponse.description) {
      alert(jsonResponse.description);
    } else if (jsonResponse.error) {
      alert(jsonResponse.error);
    } else if (jsonResponse.token) {
      setToken(jsonResponse.token);
      router.push('/app');
    }
  };
  return (
    <div className="w-full max-w-xs d-flex justify-center items-center">
      <form className="bg-zinc-700 shadow-md rounded px-8 pt-6 pb-8 mb-4" onSubmit={registerUser}>
        <div className="mb-4">
          <label className="block text-neutral-200 text-sm font-bold mb-2" htmlFor="username">
            Username
          </label>
          <input
            className='focus:outline-none focus:ring focus:ring-violet-300 shadow appearance-none bg-neutral-900 rounded w-full py-2 px-3 text-white leading-tight focus:outline-none focus:shadow-outline" id="username'
            type="text"
            name="username"
            id="username"
          />
        </div>
        <div className="mb-6">
          <label className="block text-neutral-200 text-sm font-bold mb-2" htmlFor="password">
            Password
          </label>
          <input
            className="focus:outline-none focus:ring focus:ring-violet-300 shadow appearance-none rounded w-full py-2 px-3 text-white mb-3 bg-neutral-900 leading-tight focus:outline-none focus:shadow-outline"
            type="password"
            id="password"
            name="password"
          ></input>
        </div>
        <div className="mb-5">
          <label className="block text-neutral-200 text-sm font-bold mb-2" htmlFor="password">
            Email
          </label>
          <input
            className="focus:outline-none focus:ring focus:ring-violet-300 shadow appearance-none rounded w-full py-2 px-3 text-white mb-3 bg-neutral-900 leading-tight focus:outline-none focus:shadow-outline"
            type="text"
            id="email"
            name="email"
          ></input>
        </div>
        <div className="flex items-center justify-between">
          <button
            className="focus:outline-none focus:ring focus:ring-violet-300 bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
            type="submit"
          >
            Register
          </button>
        </div>
      </form>
    </div>
  );
};
export default Register;
