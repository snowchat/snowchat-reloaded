import '../styles/globals.css';
import type { AppProps } from 'next/app';
import Link from 'next/link';
import useTokenState from '../shared/tokenState';

function NavItem({ href, children }: { href: string; children: React.ReactNode }) {
  return (
    <Link href={href}>
      <a className="bg-violet-500 hover:bg-violet-400 active:bg-violet-600 focus:outline-none focus:ring focus:ring-violet-300 p-2 rounded-md">
        {children}
      </a>
    </Link>
  );
}
function NavItemButton({ onClick, children }: { onClick: () => void; children: React.ReactNode }) {
  return (
    <button
      onClick={onClick}
      className="bg-violet-500 hover:bg-violet-400 active:bg-violet-600 focus:outline-none focus:ring focus:ring-violet-300 p-2 rounded-md"
    >
      {children}
    </button>
  );
}

function MyApp({ Component, pageProps }: AppProps) {
  if (typeof window === 'undefined') {
    return null;
  }
  const [token, setToken] = useTokenState(null);
  return (
    <div className="w-screen h-screen bg-zinc-800 flex flex-col justify-center items-center text-white">
      <nav className="flex gap-2 p-1 fixed top-0 mt-2">
        <NavItem href="/">Home</NavItem>
        {token ? (
          <>
            <NavItem href="/app">App</NavItem>
            <NavItemButton
              onClick={() => {
                localStorage.removeItem('token');
                setToken(null);
              }}
            >
              Logout
            </NavItemButton>
          </>
        ) : (
          <>
            <NavItem href="/login">Login</NavItem>
            <NavItem href="/register">Register</NavItem>
          </>
        )}
      </nav>
      <Component {...pageProps} />
    </div>
  );
}

export default MyApp;
