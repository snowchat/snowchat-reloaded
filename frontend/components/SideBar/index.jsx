import { BsPlus, BsFillLightningFill, BsGearFill } from 'react-icons/bs';
import { FaFire, FaMountain, FaPoo } from 'react-icons/fa';

const SideBar = () => {
  return (
    <div className="fixed top-0 left-0 h-screen w-16 flex flex-col
                  bg-white dark:bg-gray-900 shadow-lg">
                    
        <SideBarIcon icon={<FaMountain size={50} className={"flex justify-center"} />}  />
        <Divider />
    </div>
  );
};

const SideBarIcon = ({ icon, text = '' }) => (
  <div className="sidebar-icon group">
    {icon}
  </div>
);


const Divider = () => <hr className="sidebar-hr" />;

export default SideBar;
