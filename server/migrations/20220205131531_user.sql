CREATE OR REPLACE FUNCTION trigger_set_timestamp() RETURNS trigger AS
	$BODY$
		BEGIN
			NEW.row_updated_at = NOW();
			RETURN NEW;
		END;
	$BODY$
LANGUAGE plpgsql;


CREATE TABLE IF NOT EXISTS users (
	id				BIGINT			PRIMARY KEY,
	password_hash	CHAR(116)		NOT NULL,
	email_address	VARCHAR(100)	NOT NULL,
	username		VARCHAR(30)		NOT NULL,
	discriminator	SMALLINT		NOT NULL,
	phone_number	VARCHAR(100)	NULL,
	email_verified	BOOLEAN			NOT NULL DEFAULT FALSE,

	last_online		TIMESTAMP		NULL,
	last_ip			VARCHAR(50)		NULL,

	is_developer	BOOLEAN			NOT NULL DEFAULT FALSE,
	is_admin		BOOLEAN			NOT NULL DEFAULT FALSE,
	is_bot			BOOLEAN			NOT NULL DEFAULT FALSE,

	about_text		VARCHAR(400)	NULL,
	avatar_url		VARCHAR(120)	NULL,
	banner_url		VARCHAR(120)	NULL,

	status			SMALLINT		NOT NULL DEFAULT 0,
	status_emoji	VARCHAR(24)		NULL,
	status_expire	TIMESTAMP		NULL,
	status_text		VARCHAR(40)		NULL,

	reason_for_ban	VARCHAR(100)	NULL,
	disabled		BOOLEAN			NOT NULL DEFAULT FALSE,
	deleted			BOOLEAN			NOT NULL DEFAULT FALSE,

	tokens_from		TIMESTAMP		NOT NULL DEFAULT NOW(),
	row_created_at	TIMESTAMP		NOT NULL DEFAULT NOW(),
	row_updated_at	TIMESTAMP		NOT NULL DEFAULT NOW()
);

CREATE INDEX users_username_index ON users (username);
CREATE TRIGGER set_timestamp BEFORE UPDATE ON users FOR EACH ROW
EXECUTE PROCEDURE trigger_set_timestamp();
