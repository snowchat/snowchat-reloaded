CREATE TABLE IF NOT EXISTS guilds (
	id							BIGINT			PRIMARY KEY,
	name						VARCHAR(75) 	NOT NULL,
	owner_id					BIGINT			NOT NULL UNIQUE REFERENCES users,
	preferred_locale			VARCHAR(4)		NOT NULL DEFAULT 'en_US',
	
	rules_channel_id			BIGINT			NULL UNIQUE,
	system_msg_channel_id		BIGINT			NULL UNIQUE,
	public_updates_channel_id	BIGINT			NULL UNIQUE,

	join_msg					BOOLEAN			NOT NULL DEFAULT TRUE,
	join_msg_reply				BOOLEAN			NOT NULL DEFAULT TRUE,
	boost_msg					BOOLEAN			NOT NULL DEFAULT TRUE,

	default_msg_notif			SMALLINT		NOT NULL DEFAULT 0, -- 0 is all, 1 is only @mentions
	moderation_lvl				SMALLINT		NOT NULL DEFAULT 1, -- 0 is none, 1 is low, 2 is medium, 3 is high and 4 is highest
	explicit_lvl				SMALLINT		NOT NULL DEFAULT 2, -- 0 is no scan, 1 scan for no role, 2 is scan all
	moderation_mfa				BOOLEAN			NOT NULL DEFAULT FALSE,
	everyone_mfa				BOOLEAN			NOT NULL DEFAULT FALSE,

	max_members					INTEGER			NOT NULL DEFAULT 100000,
	member_count				INTEGER			NOT NULL DEFAULT 1,

	widget_enabled				BOOLEAN			NOT NULL DEFAULT FALSE,
	widget_channel_id			BIGINT			NULL UNIQUE,

	afk_timeout					SMALLINT		NOT NULL DEfAULT 300000, -- default is 5 minutes
	afk_channel_id				BIGINT			NULL UNIQUE,

	verified					BOOLEAN			NOT NULL DEFAULT FALSE,
	partnered					BOOLEAN			NOT NULL DEFAULT FALSE,
	official					BOOLEAN			NOT NULL DEFAULT FALSE,

	about_text					VARCHAR(400)	NULL,
	icon_url					VARCHAR(120)	NULL,
	banner_url					VARCHAR(120)	NULL,
	invite_bg_url				VARCHAR(120)	NULL,
	
	row_created_at				TIMESTAMP		NOT NULL DEFAULT NOW(),
	row_updated_at				TIMESTAMP		NOT NULL DEFAULT NOW()
);

CREATE TRIGGER set_timestamp BEFORE UPDATE ON guilds FOR EACH ROW
EXECUTE PROCEDURE trigger_set_timestamp();



CREATE TABLE IF NOT EXISTS channels (
	id				BIGINT			PRIMARY KEY,
	name			VARCHAR(75)		NOT NULL,
	guild_id		BIGINT			NOT NULL UNIQUE REFERENCES guilds ON DELETE CASCADE,

	-- 00 = Guild text
	-- 01 = Guild voice
	-- 02 = Guild category
	-- 03 = Guild stage
	-- 04 = Guild kanban
	-- 05 = Guild calendar
	-- 06 = Guild forum
	-- 07 = Guild category
	-- 08 = Guild announcement
	-- 09 = Guild checklist
	-- 10 = Guild documents
	-- 11 = Guild media
	-- 12 = DM between users
	-- 13 = Group chat
	type			SMALLINT		NOT NULL DEFAULT 0,

	perm_overwrites	BIGINT			NOT NULL DEFAULT 0,

	-- text channel
	last_message_id	BIGINT			NULL,
	topic			VARCHAR(1024)	NULL,
	rate_limit		INTEGER			NULL, -- In seconds, INTEGER has max ~596523 hours
	thread_archive	INTEGER			NOT NULL DEFAULT 86400, -- In seconds, a day

	-- voice and stage channels
	bitrate			INTEGER			NOT NULL DEFAULT 86400, -- In seconds, a day

	-- voice channel
	user_limit		SMALLINT		NULL,

	-- not category
	parent_channel	BIGINT			NULL UNIQUE REFERENCES channels,
	position		SMALLINT		NULL,

	row_created_at	TIMESTAMP		NOT NULL DEFAULT NOW(),
	row_updated_at	TIMESTAMP		NOT NULL DEFAULT NOW()
);

CREATE TRIGGER set_timestamp BEFORE UPDATE ON channels FOR EACH ROW
EXECUTE PROCEDURE trigger_set_timestamp();



CREATE TABLE IF NOT EXISTS messages (
	id				BIGINT				PRIMARY KEY,
	name			VARCHAR(75) 		NOT NULL,
	channel_id		BIGINT				NOT NULL UNIQUE REFERENCES channels ON DELETE CASCADE,
	author_id		BIGINT				NOT NULL UNIQUE REFERENCES users,
	content			VARCHAR(4096)		NOT NULL,
	row_created_at	TIMESTAMP			NOT NULL DEFAULT NOW(),
	row_updated_at	TIMESTAMP			NOT NULL DEFAULT NOW()
);

CREATE TRIGGER set_timestamp BEFORE UPDATE ON messages FOR EACH ROW
EXECUTE PROCEDURE trigger_set_timestamp();

ALTER TABLE guilds ADD FOREIGN KEY (rules_channel_id)			REFERENCES users;
ALTER TABLE guilds ADD FOREIGN KEY (rules_channel_id)			REFERENCES users;
ALTER TABLE guilds ADD FOREIGN KEY (system_msg_channel_id)		REFERENCES channels;
ALTER TABLE guilds ADD FOREIGN KEY (public_updates_channel_id)	REFERENCES channels;
ALTER TABLE guilds ADD FOREIGN KEY (widget_channel_id)			REFERENCES channels;
ALTER TABLE guilds ADD FOREIGN KEY (afk_channel_id)				REFERENCES channels;
ALTER TABLE channels ADD FOREIGN KEY (last_message_id)			REFERENCES messages;
