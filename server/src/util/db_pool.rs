use crate::util::api_error;
use once_cell::sync::OnceCell as SyncOnceCell;
use sqlx::PgPool;

pub static DATABASE_POOL: SyncOnceCell<PgPool> = SyncOnceCell::new();

// TODO: Maybe not use a global?

pub fn get_db_or_fail<'a>() -> Result<&'a PgPool, api_error::DatabaseError> {
    DATABASE_POOL
        .get()
        .ok_or_else(|| api_error::DatabaseError::Other("Database Error".to_string()))
}
