use crate::util::api_error::AuthenticationError;
use argon2::{hash_encoded, verify_encoded, Config};
use rand::{distributions::Alphanumeric, Rng};
use unicode_normalization::UnicodeNormalization;

/// Normalize a unicode string
fn normalize_string(s: String) -> String {
    s.nfkc().collect::<String>()
}

/// Generate a password hash from the supplied password, using a random salt
pub async fn verify_user_password(
    hash: String,
    password: String,
) -> Result<bool, AuthenticationError> {
    Ok(verify_encoded(
        hash.as_str(),
        normalize_string(password).as_bytes(),
    )?)
}

/// Generate a password hash from the supplied password, using a random salt
pub async fn hash_user_password(password: String) -> Result<String, AuthenticationError> {
    let salt: String = rand::thread_rng()
        .sample_iter(&Alphanumeric)
        .take(32)
        .map(char::from)
        .collect();

    let config = Config::default();
    let hash = hash_encoded(
        normalize_string(password).as_bytes(),
        salt.as_bytes(),
        &config,
    )?;

    Ok(hash)
}
