#![allow(dead_code)]

use actix_web::http::header::HeaderMap;
use hmac::{digest::KeyInit, Hmac};
use jwt::{SignWithKey, VerifyWithKey};
use serde::{Deserialize, Serialize};
use sha2::Sha256;

use crate::{
    database::models::user::{User, UserId},
    util::api_error::AuthenticationError,
};

#[derive(Serialize, Deserialize, Debug)]
pub struct Claims {
    // https://auth0.com/docs/secure/tokens/json-web-tokens/json-web-token-claims
    sub: UserId,
    iat: chrono::NaiveDateTime,
}

pub async fn create_user_token_from_id(id: UserId) -> Result<String, AuthenticationError> {
    let key: Hmac<Sha256> = Hmac::new_from_slice(dotenv::var("TOKEN_SECRET")?.as_bytes()).unwrap();
    let claims = Claims {
        sub: id,
        iat: chrono::Utc::now().naive_utc(),
    };

    let token_str = claims.sign_with_key(&key).unwrap();

    Ok(token_str)
}

pub async fn get_claims_from_token(token: &str) -> Result<Claims, AuthenticationError> {
    let key: Hmac<Sha256> = Hmac::new_from_slice(dotenv::var("TOKEN_SECRET")?.as_bytes()).unwrap();

    let claims: Claims = token.verify_with_key(&key)?;

    if claims.iat > chrono::Utc::now().naive_utc() {
        return Err(AuthenticationError::JWTTimeError);
    }

    Ok(claims)
}

pub async fn get_user_id_from_token(token: &str) -> Result<UserId, AuthenticationError> {
    let claims = get_claims_from_token(token).await?;

    Ok(claims.sub)
}

pub async fn get_user_from_token<'a, 'b, E>(
    token: &str,
    executor: E,
) -> Result<User, AuthenticationError>
where
    E: sqlx::Executor<'a, Database = sqlx::Postgres>,
{
    let claims = get_claims_from_token(token).await?;

    let res = User::get_from_id(claims.sub, executor).await?;

    match res {
        Some(result) => {
            if claims.iat < result.tokens_from {
                Err(AuthenticationError::JWTTimeError)
            } else {
                Ok(result)
            }
        }
        None => Err(AuthenticationError::InvalidCredentialsError),
    }
}

pub async fn get_user_from_headers<'a, 'b, E>(
    headers: &HeaderMap,
    executor: E,
) -> Result<User, AuthenticationError>
where
    E: sqlx::Executor<'a, Database = sqlx::Postgres>,
{
    let token = headers
        .get("Authorization")
        .ok_or(AuthenticationError::InvalidCredentialsError)?
        .to_str()
        .map_err(|_| AuthenticationError::InvalidCredentialsError)?;

    Ok(get_user_from_token(token, executor).await?)
}

pub async fn get_claims_from_headers(headers: &HeaderMap) -> Result<Claims, AuthenticationError> {
    let token = headers
        .get("Authorization")
        .ok_or(AuthenticationError::InvalidCredentialsError)?
        .to_str()
        .map_err(|_| AuthenticationError::InvalidCredentialsError)?;

    Ok(get_claims_from_token(token).await?)
}

pub async fn get_user_id_from_headers(headers: &HeaderMap) -> Result<UserId, AuthenticationError> {
    let token = headers
        .get("Authorization")
        .ok_or(AuthenticationError::InvalidCredentialsError)?
        .to_str()
        .map_err(|_| AuthenticationError::InvalidCredentialsError)?;

    Ok(get_user_id_from_token(token).await?)
}
