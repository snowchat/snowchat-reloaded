use actix_web::{post, web, HttpRequest, HttpResponse};
use lazy_static::lazy_static;
use regex::Regex;
use serde::{Deserialize, Serialize};
use serde_json::json;
use validator::Validate;

use crate::{
    types::IdGeneratorData,
    util::{
        api_error::{ApiError, AuthenticationError, DatabaseError},
        auth::create_user_token_from_id,
        db_pool::get_db_or_fail,
        password_helpers,
        validate::validation_errors_to_api_error,
    },
};

lazy_static! {
    pub static ref RE_USERNAME: Regex = Regex::new(r#"^[a-zA-Z0-9!@$()`.+,_" -]*$"#).unwrap();
}

#[derive(Debug, Validate, Serialize, Deserialize)]
pub struct CreateUserStruct {
    #[validate(
        length(min = 3, max = 30, message = "Length out of bounds 3-30"),
        regex(path = "RE_USERNAME", message = "Invalid username")
    )]
    username: String,
    #[validate(length(min = 3, max = 30, message = "Length out of bounds 3-30"))]
    password: String,
    #[validate(
        length(min = 3, max = 100, message = "Length out of bounds 3-100"),
        email(message = "Invalid E-Mail address")
    )]
    email_address: String,
}

// TODO: hCaptcha

#[post("/register")]
pub async fn route(
    data: web::Json<CreateUserStruct>,
    req: HttpRequest,
    id_generator_wrap: IdGeneratorData,
) -> Result<HttpResponse, ApiError> {
    data.validate().map_err(validation_errors_to_api_error)?;

    if req.headers().contains_key("Authorization") {
        return Err(AuthenticationError::Other("You are already logged in!".into()).into());
    }
    let db = get_db_or_fail()?;

    let email_count_data = sqlx::query!(
        "
    SELECT COUNT(email_address) FROM users WHERE email_address = $1
    ",
        data.email_address.clone()
    )
    .fetch_one(db)
    .await?;

    if email_count_data.count.unwrap_or(0) != 0 {
        // There is already an user with that email address
        return Err(ApiError::InvalidInputError(
            "There is already an user with that email address".into(),
        ));
    }

    let password_hash = password_helpers::hash_user_password(data.password.clone()).await?;

    let mut id_generator = id_generator_wrap
        .lock()
        .map_err(|_| DatabaseError::IdGenerationError)?;
    let id = id_generator.generate();

    // TODO: generate discriminator
    let discriminator = 0;

    sqlx::query!("INSERT INTO users (id, password_hash, email_address, username, discriminator) VALUES ($1, $2, $3, $4, $5)", id, password_hash, data.email_address.clone(), data.username, discriminator).execute(db).await?;

    let (message, check_email) = {
        let var = dotenv::var("SIGNUP_EMAIL")?;
        let var = var.as_str();
        match var {
            "true" | "1" | "yes" => {
                (format!("A verification email has been sent to: {}. The link will only be valid for an hour.", data.email_address), true)
            }
            _ => {
                ("Your account has been successfully registered".to_string(), false)
            }
        }
    };

    let token = create_user_token_from_id(id).await?;

    Ok(HttpResponse::Ok().json(json!({
        "success": true,
        "id": id,
        "message": message,
        "check_email": check_email,
        "token": token
    })))
}
