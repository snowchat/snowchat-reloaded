use actix_web::{post, web, HttpRequest, HttpResponse};
use serde::{Deserialize, Serialize};
use serde_json::json;
use validator::Validate;

use crate::util::{
    api_error::{ApiError, AuthenticationError},
    auth::create_user_token_from_id,
    db_pool::get_db_or_fail,
    password_helpers::verify_user_password,
    validate::validation_errors_to_api_error,
};

#[derive(Debug, Validate, Serialize, Deserialize)]
pub struct LoginUserStruct {
    #[validate(length(min = 3, max = 30, message = "Length out of bounds 3-30"))]
    password: String,
    #[validate(
        length(min = 3, max = 100, message = "Length out of bounds 3-100"),
        email(message = "Invalid E-Mail address")
    )]
    email_address: String,
}

// TODO: Validate
// TODO: hCaptcha

#[post("/login")]
pub async fn route(
    data: web::Json<LoginUserStruct>,
    req: HttpRequest,
) -> Result<HttpResponse, ApiError> {
    data.validate().map_err(validation_errors_to_api_error)?;

    if req.headers().contains_key("Authorization") {
        return Err(AuthenticationError::Other("You are already logged in!".into()).into());
    }
    let db = get_db_or_fail()?;

    let partial_user = sqlx::query!(
        "
    SELECT id, password_hash FROM users WHERE email_address = $1
    ",
        data.email_address.clone()
    )
    .fetch_one(db)
    .await
    .ok();

    if let Some(partial_user) = partial_user {
        if !verify_user_password(partial_user.password_hash, data.password.clone()).await? {
            return Err(AuthenticationError::InvalidCredentialsError.into());
        }

        let token = create_user_token_from_id(partial_user.id).await?;

        Ok(HttpResponse::Ok().json(json!({
            "success": true,
            "id": partial_user.id,
            "token": token
        })))
    } else {
        Err(AuthenticationError::InvalidCredentialsError.into())
    }
}
