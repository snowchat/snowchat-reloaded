pub mod check;
pub mod confirmation;
pub mod login;
pub mod register;
use actix_web::{web, Scope};

pub fn scope() -> Scope {
    web::scope("/auth")
        .service(check::route)
        .service(register::route)
        .service(login::route)
}
