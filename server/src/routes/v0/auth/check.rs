use crate::util::{auth::get_user_from_headers, db_pool::get_db_or_fail};
use actix_web::{get, HttpRequest, HttpResponse};
use serde_json::json;

use crate::util::api_error::ApiError;

#[get("/check")]
pub async fn route(req: HttpRequest) -> Result<HttpResponse, ApiError> {
    let db = get_db_or_fail()?;
    let user = get_user_from_headers(req.headers(), db).await;

    match user {
        Ok(user) => {
            let json = json!({
                "id": user.id,
                "username": user.username,
                "email_address": user.email_address,
                "phone_number": user.phone_number,
                "is_developer": user.is_developer
            });
            Ok(HttpResponse::Ok().json(json))
        }
        Err(err) => {
            let err: ApiError = err.into();
            let err = err.error_struct();
            let json = json!({ "error": err });
            Ok(HttpResponse::Ok().json(json))
        }
    }
}
