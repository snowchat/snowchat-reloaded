pub mod auth;

use actix_web::{get, web, HttpResponse, Scope};
use serde_json::json;

#[get("/")]
async fn route_index_get() -> HttpResponse {
    HttpResponse::Ok().json(json!({
        "message": "Please check the docs :)"
    }))
}

pub fn scope() -> Scope {
    web::scope("")
        .service(auth::scope())
        .service(route_index_get)
}
