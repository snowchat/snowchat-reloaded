use crate::util::{api_error::ApiError, db_pool::get_db_or_fail};
use actix_web::{get, web, HttpResponse, Responder};
use futures::TryStreamExt;
use serde_json::json;
use sqlx::Row;

mod v0;

#[get("/username")]
async fn username() -> Result<HttpResponse, ApiError> {
    let db = get_db_or_fail()?;
    let mut transaction = db.begin().await?;

    let query = sqlx::query(concat!(
        "SELECT usename AS username,",
        "    CASE ",
        "       WHEN usesuper AND usecreatedb THEN ",
        "         CAST('superuser, create database' AS pg_catalog.text)",
        "       WHEN usesuper THEN ",
        "          CAST('superuser' AS pg_catalog.text)",
        "       WHEN usecreatedb THEN ",
        "          CAST('create database' AS pg_catalog.text)",
        "       ELSE ",
        "          CAST('' AS pg_catalog.text)",
        "    END role_attributes",
        "  FROM pg_catalog.pg_user",
        "  ORDER BY username desc;"
    ));

    let username = {
        let mut rows = query.fetch(&mut transaction);
        let row = rows.try_next().await?.unwrap();
        let username: String = row.try_get("username")?;
        username
    };

    transaction.commit().await?;
    Ok(HttpResponse::Ok().json(json!({ "username": username })))
}

#[get("/health")]
async fn route_health_get() -> impl Responder {
    "OK".to_string()
}

#[get("/")]
async fn route_index_get() -> HttpResponse {
    HttpResponse::Ok().json(json!({
        "name": "snowchat",
        "version": env!("CARGO_PKG_VERSION"),
        "message": "Welcome to the API!"
    }))
}

pub fn config(cfg: &mut web::ServiceConfig) {
    cfg.service(
        web::scope("/api")
            .service(username)
            .service(web::scope("/latest").service(v0::scope()))
            .service(web::scope("/v0").service(v0::scope())),
    )
    .service(route_index_get)
    .service(route_health_get);
}
