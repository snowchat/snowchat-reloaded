use actix_cors::Cors;
use actix_web::{http, web, App, HttpServer};
use log::info;
use snowflake::SnowflakeIdGenerator;
use std::sync::Mutex;

use util::db_pool::DATABASE_POOL;

mod database;
mod routes;
mod types;
mod util;

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    dotenv::dotenv().ok();
    env_logger::Builder::from_env(env_logger::Env::default().default_filter_or("info")).init();
    info!("Starting Snowchat's Rust-based server");
    database::check_for_migrations()
        .await
        .expect("An error occurred while running migrations.");

    // Database Connector
    let pool = database::connect()
        .await
        .expect("Database connection failed");

    DATABASE_POOL
        .set(pool.clone())
        .unwrap_or_else(|_| panic!("Failed to set the database global"));

    // Snowlake ID generator
    let id_generator = SnowflakeIdGenerator::new(0, 0);

    HttpServer::new(move || {
        App::new()
            .wrap(
                Cors::default()
                    .allowed_methods(["GET", "POST", "DELETE", "PATCH", "PUT"])
                    .allowed_headers([http::header::AUTHORIZATION, http::header::ACCEPT])
                    .allowed_header(http::header::CONTENT_TYPE)
                    .allow_any_origin()
                    .max_age(3600),
            )
            //.app_data(web::Data::new(pool.clone()))
            .app_data(web::Data::new(Mutex::new(id_generator)))
            .configure(routes::config)
    })
    .bind(dotenv::var("BIND_ADDR").unwrap())?
    .run()
    .await
}
