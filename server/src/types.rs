use std::sync::Mutex;

use actix_web::web::Data;
use snowflake::SnowflakeIdGenerator;

pub type IdGeneratorData = Data<Mutex<SnowflakeIdGenerator>>;
