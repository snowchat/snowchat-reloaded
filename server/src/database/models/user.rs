use serde::{Deserialize, Serialize};

// TODO: bigdecimal (Rust) aka numeric (PG) id

pub type UserId = i64;

#[derive(Serialize, Deserialize, Clone)]
pub struct User {
    pub id: UserId,
    pub password_hash: String,
    pub email_address: String,
    pub username: String,
    pub discriminator: i16,
    pub phone_number: Option<String>,
    pub email_verified: bool,

    pub last_online: Option<chrono::NaiveDateTime>,
    pub last_ip: Option<String>,

    pub is_developer: bool,
    pub is_admin: bool,
    pub is_bot: bool,

    pub about_text: Option<String>,
    pub avatar_url: Option<String>,
    pub banner_url: Option<String>,

    pub status: i16,
    pub status_emoji: Option<String>,
    pub status_expire: Option<chrono::NaiveDateTime>,
    pub status_text: Option<String>,

    pub reason_for_ban: Option<String>,
    pub disabled: bool,
    pub deleted: bool,

    pub tokens_from: chrono::NaiveDateTime, // TODO: Use access/refresh tokens
    pub row_created_at: chrono::NaiveDateTime,
    pub row_updated_at: chrono::NaiveDateTime,
}

impl User {
    pub async fn get_from_id<'a, 'b, E>(
        id: i64,
        executor: E,
    ) -> Result<Option<Self>, sqlx::error::Error>
    where
        E: sqlx::Executor<'a, Database = sqlx::Postgres>,
    {
        let result = sqlx::query_as!(
            User,
            "
            SELECT *
            FROM users u
            WHERE u.id = $1
            ",
            id,
        )
        .fetch_optional(executor)
        .await?;

        if let Some(row) = result {
            Ok(Some(row))
        } else {
            Ok(None)
        }
    }
}
