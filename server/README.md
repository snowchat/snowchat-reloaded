## Setup
```shell
cp default.env .env
cargo sqlx database setup
```

## Production
```shell
# Build requires query checks
docker-compose -f ../docker-compose.yml up -d
cargo build --release
docker-compose -f ../docker-compose.yml down

./target/release/snowchat_server
```

## Development
```shell
# Build requires query checks and run well.. connects to the database
docker-compose -f ../docker-compose.yml up -d
cargo run
```

## Reset database
```sql
DROP DATABASE snowchat WITH (FORCE);
```
```shell
cargo sqlx database setup
```
